module.exports = function getSidebar (name, lang) {
	if (lang === 'fr') {
		return [
			{
				title: name,
				collapsable: false,
				sidebarDepth: 2,
				children: [
					'',
					'formulaire-web'
				]
			}
		]
	}

	return [
		{
			title: name,
			collapsable: false,
			sidebarDepth: 2,
			children: [
				'',
				'web-form'
			]
		}
	]
}
# Plan comptable

Pour accéder au plan comptable, faire *Comptabilité>Plan Comptable*. Le plan comptable est la structure comptable de la société. Il organise les différents comptes de l’entreprises selon différentes catégories. La structure de ce plan comptable dépend du choix fait lors de l’installation de Dokos.

## 1. Modifier un compte
1.1 Appuyer sur l’onglet modifier

1.2 Choisir un compte parent. Un compte parent permet de hiérarchiser la structure du plan comptable.

1.3 Choisir un type de compte. Dokos utilise le type de compte pour le proposer lors des transactions.

1.4 Dokos propose de geler les comptes si besoin afin de restreindre les écritures à un nombre restreint d’utilisateurs.

1.5 Paramétrer le solde en débit ou en crédit.
 
## 2. Ajouter une sous-catégorie
2.1 Remplir le champ Nom du Compte.

2.2 Remplir le numéro du compte.  Ce numéro aura pour préfixe le numéro de la catégorie dans laquelle on crée le compte.

2.3 Possibilité de rassembler des comptes dans un même compte en cochant « Est un groupe ».

2.4 Choisir un type de compte. Dokos utilise le type de compte pour le proposer lors des transactions.

2.5 Choisir la devise (ex : EUR). Dokos remplira automatiquement avec la devise choisie lors de l’installation. Cette option permet de changer la devise « par défaut » pour un compte particulier.
 
## 3. Voir le livre comptable

Selectionner « View Ledger ».

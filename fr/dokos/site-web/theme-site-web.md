# Thème du Site Web

Vous pouvez modifier l'apparence de chaque **page de votre site web** à l’aide de **thèmes** personnalisés.

### Créer un nouveau thème

1. Allez à: Site web > Configuration > Thème du Site web  > Nouveau·elle.(en haut à droite)
2. Vous pouvez définir votre thème dans l’espace de code associé, en langage CSS. 
3. Dans le champ situé juste en-dessous, vous pouvez indiquer l’URL d’un page de style CSS déjà existante. 
On peut utiliser l’éditeur de thème (bouton en haut à droite, un peu sous le bouton “nouveau”) en cliquant sur “configurer le thème”. On peut alors directement choisir sa police, ainsi que les couleurs des différents éléments du site.

module.exports = function getSidebar (name, lang) {
	if (lang === 'fr') {
		return [
			{
				title: name,
				collapsable: false,
				children: [
					'',
					'devis'
				]
			}
		]
	}

	return [
		{
			title: name,
			collapsable: false,
			children: [
				'',
				'quotation'
			]
		}
	]
}
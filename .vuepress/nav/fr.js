module.exports = [
	{
		text: 'dokos',
		link: '/fr/dokos/',
		items: [
			{
				text: 'Démarrer avec dokos',
				items: [
					{
						text: 'Installer dokos',
						link: '/fr/dokos/installation/'
					}
				]
			},
			{
				text: 'Modules',
				items: [
					{
						text: 'Comptabilité',
						link: '/fr/dokos/comptabilite/'
					},
					{
						text: 'Site web',
						link: '/fr/dokos/site-web/'
					},
					{
						text: 'Stocks',
						link: '/fr/dokos/stocks/'
					},
					{
						text: 'Vente',
						link: '/fr/dokos/vente/'
					}
				]
			},
			{
				text: 'Publications',
				items: [
					{
						text: 'Versions',
						link: '/fr/dokos/versions/'
					}
				]
			},
		]
	}
]
# Diaporama du site web

Vous pouvez créer des diaporamas pour les intégrer sur votre site (utile par exemple pour les bannières).
 
### Créer un nouveau diaporama
 
Allez à: **Site web > Site Internet  > Diaporama du site Web > Nouveau·elle.** (en haut à droite).
Dans article du diaporama, ajoutez autant de lignes que vous souhaitez avoir d’images dans votre diaporama , puis ajoutez des images en liant des liens URL ou en chargeant directement une image sauvegardée sur votre machine.
Pour chaque image vous pouvez y associer un titre et une description.

![Titre et descritpion de diaporama](.vuepress/public/images/website/website_slideshow/diaporama-titre-description.png)


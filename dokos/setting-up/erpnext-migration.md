---
tags: ["setting up", "migration"]
---
# Migrate from ERPNext
<TagLinks />

- This migration guide has been tested for ERPNext v11.x.x and v12.x.x
- It is therefore recommended that you upgrade your ERPNext instance to v11.x.x or v12.x.x before following this guide


## Pre-requisites

Your __bench__ environment needs to run on Python 3 (minimum Python 3.6).

If that is not the case, run the following command from your bench folder (`home/{user}/{your_bench}):

`bench migrate-env python3`

:::warning
dokos is not compatible with Python 2.7
:::


## Switch to dokos

### Change the default repository URL and update __bench__

- In your user's local folder, go to your __bench application__ folder  

```
cd /home/{user}/.bench
```

or

```
cd /home/{user}/bench-repo
```

Usually your user will named be `frappe`, but it can be any user you have created when installing Frappe/ERPNext  

- Change the remote URL and update the application:

```
git remote set-url origin https://gitlab.com/dokos/docli.git
git pull origin master
```

### Change the default repository URL for each application

- From your bench folder - usually `/home/frappe/frappe-bench` - run the following commands  

```
bench remote-reset-url dodock
bench remote-reset-url dokos
```


- Then switch to branch __master__ to setup a production environment

```
bench switch-to-master
```

:::tip
The applications Frappe and ERPNext are not yet renamed to dodock and dokos in __bench__.
Therefore, except for resetting the remote url and installing an application, you still need to call each application with their initial names.
:::

### Check that the application is ready to be upgraded

- From your bench folder - usually `/home/frappe/frappe-bench` - run the following command

```
bench remote-urls
```

The output should contain:

```
frappe   https://gitlab.com/dokos/dodock.git
erpnext  https://gitlab.com/dokos/dokos.git
```

### Upgrade your application

- From your bench folder - usually `/home/frappe/frappe-bench` - run the following command

```
bench update
```

If you encounter any issue while upgrading, please open a new issue in the [dokos issue tracker](https://gitlab.com/dokos/dokos/issues)
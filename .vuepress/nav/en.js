module.exports = [
	{
		text: 'dokos',
		link: '/dokos/',
		items: [
			{
				text: 'Quick Start',
				items: [
					{
						text: 'Install dokos',
						link: '/dokos/setting-up/'
					}
				]
			},
			{
				text: 'Modules',
				items: [
					{
						text: 'Accounting',
						link: '/dokos/accounting/'
					},
					{
						text: 'Selling',
						link: '/dokos/selling/'
					},
					{
						text: 'Stocks',
						link: '/dokos/stocks/'
					},
					{
						text: 'Website',
						link: '/dokos/website/'
					}
				]
			},
			{
				text: 'Releases',
				items: [
					{
						text: 'Versions',
						link: '/dokos/versions/'
					}
				]
			}
		]
	}
]
# Créer une catégorie d’actif

**Prérequis : créer [un livre comptable](fr/dokos/comptabilite/plan-comptable.md).**

- Aller dans Actifs, cliquer sur Catégorie d’Actif situé dans Actifs.
- Cliquer sur “Créer une nouvelle catégorie d’actif”.
- Entrer le nom du livre, puis remplir les “Détails du livre comptable” avec au minimum les éléments suivants : associer le livre comptable correspondant, une méthode d’amortissement, nombre total d’amortissement, fréquence des amortissements.
- Remplir le cadre Comptes avec au moins le nom de la société et le Compte d’Actif Immobilier.

# Créer un nouvel actif

De manière générale, lors de l’instanciation d’un objet (ici un actif), il est nécessaire d’avoir des objets pré requis (ici un lieu, un article (immobilisation), catégorie d’actif).
Commençons par créer un article. Deux solutions sont possibles si aucun article n’est suggéré. Créer le nouvel article à partir de l’onglet s’affichant en dessous (voir image) ou alors aller dans Achat et créer un article préalablement. Attention : cet article doit respecter dans tous les cas les filtres imposés. Ici : Désactiver = 0, Est Immobilisation = 1, Maintenir Stock = 0.

De plus, l’article doit être associé à au moins une facture d’achat.

- Cliquer sur “créer un nouvel actif”
- Entrer le nom de l’actif
- Remplir les champs correspondants:

![champs-actifs](.vuepress/public/images/fr/comptabilite/actifs/champs-actifs.md)



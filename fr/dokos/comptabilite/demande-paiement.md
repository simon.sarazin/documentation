# Demande de paiement

Une demande de paiement est un document permettant l'envoi d'un lien de paiement à vos clients afin de leur permettre de payer leurs factures directement sur votre site web dokos.

### 1. Créer une demande de paiement

1. Depuis une commande client ou une facture soumise, mais impayée, cliquez sur 'Créer > Demande de paiement'.
2. Ajoutez une ou plusieurs passerelles de paiement.
3. Sélectionez un modèle d'email ou rédigez un message si vous souhaitez envoyer un email à votre client à la validation du document.
4. Enregistrez et validez votre document.

### 2. Fonctionnalités
#### 2.1 Passerelles de paiement

Vous devez sélectionner au moins une passerelle de paiement afin de pouvoir soumettre votre demande de paiement.
dokos crée un lien unique pour chaque requête de paiement redirigeant vos utilisateurs vers une page de paiement sur laquelle ils peuvent choisir leur méthode de paiement préférée:
![dokos payment page](/images/accounting/payment_request/payment_request_link.png)

Si votre type de document de référence (Facture/bon de commande) est lié à un abonnement, dokos effectue les actions suivantes:
1. Sélection automatique des méthodes de paiement lorsque vous enregistrez le document.
2. Vérification de la cohérence avec les passerelles de paiement des plans d'abonnements liés au document de référence si vous ajoutez ou supprimez une passerelle de paiement.

:::tip À savoir
Si vous utilisez le prélèvement Sepa avec GoCardless, une fois que le client est lié à un mandat valide, vous pouvez obtenir le paiement directement depuis la demande de paiement sans avoir à envoyer un lien à votre client.

Cliquez sur 'Action > Obtenir le paiement immédiatement' pour lancer le paiement.

Si vous décidez tout de même d'envoyer un lien à votre client, le lien vers la passerelle de paiement GoCardless n'apparaîtra pas à l'écran si l'utilisateur est lié avec un mandat valide. Pour créer un nouveau client GoCardless, annulez d'abord tous les mandats existants dans dokos.
:::

#### 2.2 Modèle de passerelles de paiement

Afin de faciliter la sélection de plusieurs passerelles de paiement pour chaque demande de paiement, créez des modèles et sélectionnez uniquement le modèle correpondant à votre cas d'utilisation à chaque fois.


#### 2.3 Montant des frais 

Vous pouvez soit entrer un montant de frais manuellement ou laisser 0 pour que dokos récupère le montant exact des frais prélevés par l'intermédiaire de paiement et l'enregistrer automatiquement.

Ce montant sera ensuite déduit du montant payé et comptabilisé sur le compte de frais enregistré dans la passerelle de [paiement correspondante](/fr/dokos/comptabilite/passerelle-paiement.md)).

#### 2.4 Notification par email

Vous pouvez envoyer un email de notification directement depuis une demande de paiement à votre client.
Sélectionnez un modèle d'email ou rédigez un nouveau message directement dans le champ message, sélectionnez un format d'impression pour votre document de référence et enregistrez puis validez simplement votre document pour que l'email soit automatiquement envoyé au destinataire.

Vous pouvez utiliser deux objets spécifiques et une variable pour créer un email dynamique:

::: v-pre
- Objets:
    - **doc**: donne accès au document de demande paiement. Exemple: `{{ doc.transaction_date }}`
    - **reference**: donne accès au document de référence (Commande client/facture). Exemple: `{{ reference.grand_total }}`

- Variable:
    - **payment_link**: donne accès au lien de paiement pré-formatté.  
    Exemple: `{{ payment_link }}` sera transformé en `https://{your_site}/payments?link=3bec23ddb9e0f4d4af1df01d807725680544a603fa500228085ac65f`
:::

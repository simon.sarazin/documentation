---
tags: ["installation", "migration"]
---
# Migrer depuis ERPNext
<TagLinks />

- Ce guide de migration a été testé pour ERPNext v11.x.x et v12.x.x
- Il est recommandé de mettre à jour ERPNext vers la version v11.x.x ou v12.x.x avant de suivre ce guide


## Pré-requis

Votre environnement __bench__ doit fonctionner avec Python 3 (Python 3.6 minimum).

Si ce n'est pas le cas, lancez la commande suivante depuis votre dossier bench (`home/{utilisateur}/{votre_bench}):

`bench migrate-env python3`

:::warning
dokos est incompatible avec Python 2.7
:::


## Passer sur dokos

### Changez l'URL de dépôt et mettre à jour __bench__

- Dans le dossier local de votre utilisateur, allez dans le dossier de l'application __bench__  

```
cd /home/{utilisateur}/.bench
```

ou  

```
cd /home/{utilisateur}/bench-repo
```

Généralement votre utilisateur est appelé `frappe`, mais cela peut aussi être un autre utilisateur, créé lors de l'installation de Frappe/ERPNext  

- Changez l'URL du dépôt et mettez à jour l'application:

```
git remote set-url origin https://gitlab.com/dokos/docli.git
git pull origin master
```

### Changez l'URL dépôt par défaut de chaque application

- Depuis votre dossier bench - généralement `/home/frappe/frappe-bench` - lancez les commandes suivantes :

```
bench remote-reset-url dodock
bench remote-reset-url dokos
```


- Ensuite migrez sur la branche __master__ pour configurer l'environnement de production :

```
bench switch-to-master
```

:::tip
Les applications Frappe et ERPNext ne sont pas encore renommées en dodock et dokos dans __bench__.
De ce fait, à l'exception de la modification des URL de dépôts et l'installation d'applications, vous devez encore appeler chaque application par son nom d'origine.
:::

### Vérifier que l'application est prête à être mise à jour

- Depuis votre dossier bench - généralement `/home/frappe/frappe-bench` - lancez la commande suivante :

```
bench remote-urls
```

Le résultat doit contenir :

```
frappe   https://gitlab.com/dokos/dodock.git
erpnext  https://gitlab.com/dokos/dokos.git
```

### Mettre à jour l'application

- Depuis votre dossier bench - généralement `/home/frappe/frappe-bench` - lancez la commande suivante :

```
bench update
```

Si vous rencontrez des problèmes lors de la migration, veuillez ouvrir un ticket dans le système de suivi des [tickets dokos](https://gitlab.com/dokos/dokos/issues)
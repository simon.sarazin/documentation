# Paramétrer les informations comptables de sa société
## 1-      Tableaux de bord
Un récapitulatif du nombre de devis, de commandes, de bons de livraison, de factures, de projets et un graphique représentant le nombre de ventes mensuel.
En appuyant sur les • à côté des différents items, il est possible de réaliser un ajout.
## 2-      Valeurs par défaut
1. Remplir le champ « en-tête courrier par défaut » avec l’en-tête de ma société. Pour créer une nouvelle en-tête, appuyez sur la flèche à droite de la barre de saisie.
2. * Remplir le champ « termes et conditions applicables à la vente ».  Pour créer un nouveau dossier « Termes et conditions », appuyez sur le + qui apparait en dessous de la barre de saisie.
    ![Nouvelle société](.vuepress/public/images/accounting/setting_accountancy_configuration/nouvelle-societe.JPG)
   *  Remplir les champs comme indiqué sur la capture ci-dessous :
    
![RIB de la société](.vuepress/public/images/accounting/setting_accountancy_configuration/societe-rib.jpg)
   *  Les autres champs de cette partie peuvent être complétés si besoin, ou sont automatiquement remplis.
3. Paramètres des ventes
   * Fixez le champ « Objectif de Vente Mensuel » afin de pouvoir comparer facilement ces résultats avec les objectifs fixés.
   * Dokos remplit automatiquement le champ « Total des ventes mensuelles ».

4. Paramètres des comptes
   * Compte bancaire par défaut : par défaut il s’agit de celui rentré lors de la première visite sur Dokos. Il est possible de le changer en appuyant sur la flèche à droite de la barre de saisie.
   * La plupart des autres comptes sont à rentrer manuellement. Il faut faire des choix selon plusieurs options issues du plan comptable. Parfois, il y a des suggestions automatiques. Les choix faits dans la capture ci-dessous sont un exemple de choix cohérent :
    ![Paramètres des comptes de société](.vuepress/public/images/accounting/setting_accountancy_configuration/parametres-societe.JPG)
 
Dans les cas où il n’y a pas de suggestions automatiques, il faut créer un compte en appuyant sur la flèche à droite de la barre de saisie. Pour plus d’explication, voir l’onglet « plan comptable ».
 
5. Paramètres des stocks
Certains comptes doivent être renseignés manuellement. Dokos propose des suggestions automatiques. Les choix faits dans la capture ci-dessous sont un exemple de choix cohérent :
![Paramètres des stocks](.vuepress/public/images/accounting/setting_accountancy_configuration/parametres-stocks.JPG)

6. Paramètres d’amortissement des immobilisations
Certains comptes doivent être renseignés manuellement. Dokos propose des suggestions automatiques. Les choix faits dans la capture ci-dessous sont un exemple de choix cohérent :
![Paramètres d'amortissement des immobilisations](.vuepress/public/images/accounting/setting_accountancy_configuration/parametres-amortissement-immo.JPG)
 
7. Informations sur la société
Il est possible de remplir des informations supplémentaires sur la société.
 

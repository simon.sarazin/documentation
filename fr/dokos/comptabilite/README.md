# Tables des matières

- [Actifs](/fr/dokos/comptabilite/actifs.md)
- [Facture](/fr/dokos/comptabilite/facture-vente.md)
- [Demande de paiement](/fr/dokos/comptabilite/demande-paiement.md)
- [Prélèvement Sepa](/fr/dokos/comptabilite/prelevement-sepa.md)
- [Passerelles de paiement](/fr/dokos/comptabilite/passerelles-paiement.md)
- [Paramétrage des informations comptables de la société](/fr/dokos/comptabilite/parametrage-informations-comptables.md)
- [Fournisseur](/fr/dokos/comptabilite/fournisseur.md)
- [Plan comptable](/fr/dokos/comptabilite/plan-comptable.md)
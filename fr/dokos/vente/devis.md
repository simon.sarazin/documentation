# Devis

**Un devis est un coût estimé des produits/services que vous souhaitez vendre à vos clients actuels/futurs.** 

Pour accéder à la liste de devis, allez à:
> Vente > Devis

Un devis contient des détails sur:

  * Le destinataire du devis.
  * Les articles et quantités que vous proposez.
  * Le prix auquel ils sont proposés.
  * Les taxes applicables.
  * Les autres frais (livraison, assurance) le cas échéant.
  * La validité du contrat.
  * Le moment de la livraison.
  * Toutes autres conditions.

:::tip Astuce
Les images sont souvent appréciées sur un devis. Faites en sorte que vos articles aient une image associée.
:::

## 1. Pré-requis
Avant de créer un devis, il est recommandé de créer les éléments suivants:

* [Client](/fr/dokos/vente/client)
* [Prospect](/fr/dokos/vente/prospect)
* [Article](/fr/dokos/stock/article)

## 2. Créer un devis
1. Allez sur la liste des devis et cliquez sur "Nouveau".
2. Sélectionnez le tiers auquel le devis est envoyé (Client/Prospect) dans le champ "Type de tiers concerné".
3. Choisissez un prospect/client.
4. Entrez une date de fin de validité pour ce devis.
5. Le type de commande peut être Ventes, Entretien ou Panier. Panier est spécifique aux ventes via le site web, vous devez donc choisir entre Ventes et Entretien: Ventes concerne principalement les commandes avec livraison, Entretien les commandes sans livraison associée.
6. Ajoutez les articles et leurs quantités dans le tableau d'article. Les prix seront récupérés automatiquement depuis le prix de l'article. Vous pouvez aussi récupérer des articles depuis une opportunité en cliquant sur `Obtenir les articles de > Opportunité`.
7. Ajoutez les taxes et frais additionnels applicables.
8. Enregistrez.

Vous pouvez aussi créer un devis depuis une opportunité en cliquant sur le bouton `Créer > Devis`.

## 3. Fonctionnalités

### 3.1. Adresse et contact
Dans cette section, vous trouverez quatre champs importants:

* **Adresse du client:** C'est l'adresse de facturation du client.
* **Adresse de livraison:** Adresse à laquelle les articles seront expédiés.
* **Personne de contact:** Si votre client est une organisation, vous pouvez ajouter la personne à contacter dans ce champ.
* **Territoire:** Région à laquelle le client appartient. Par défaut c'est "Tous les secteurs". 

### 3.2. Devise et liste de prix

Vous pouvez définir la devise dans laquelle sera envoyée le devis.

Les prix des articles seront récupérés à partir de la liste de prix sélectionnée.  
La liste de prix est récupérée automatiquement à partir des données de base du client, puis du groupe de client et enfin de la société si elle n'était pas définie avant.
En cochant "Ignorer les règles de tarification", le système n'appliquera aucune des règles de tarification qui auraient pu s'appliquer sur ce devis.

### 3.3. Le tableau d'articles
Ce tableau peut être ouvert en cliquant sur le triangle inversé au bout de chaque ligne du tableau.

* Lors de la sélection d'un code d'article, les champs suivants sont récupérés automatiquement: nom de l'article, description, une image si attachée, une quantité par défaut de 1 et le prix unitaire. Vous pouvez ajouter des remises et calculer une marge dans la section correspondante.
* Dans la **Section Remise et Marge**, vous pouvez ajouter une marge additionnelle sur l'article ou faire une remise. Ces deux fonctionnalités fonctionnent en montant ou en pourcentage. Le prix unitaire final sera affiché dans la section du dessous. Vous pouvez attribuer un modèle de taxe d'article spécifique à chaque article.
* Le **Poids de l'article** est récupéré si défini dans les données de base de l'article.
* Dans **Entrepôt et Référénce**, l'entrepôt de stockage des articles est récupéré depuis les données de base de l'article.
* Dans **Planification**, vous pouvez voir les quantités projetées et les quantités réelles de stock. Si vous cliquez sur le bouton 'Solde du stock', vous serez redirigés vers le rapport de solde du stock.
* Dans **Panier**, les notes additionnelles sont celles ajoutées par les clients dans leur panier.  
* **Saut de page** créera un saut de page juste avant cet article dans le format d'impression.

* Vous pouvez insérer des lignes au-dessus/en-dessous, dupliquer, déplacer ou supprimer des lignes dans ce tableau.

:::tip Astuce
Vous pouvez télécharger vos articles au format CSV et les télécharger dans une autre transaction.
:::

La quantité totale, le prix unitaire et le poids net de tous les articles sont affichés sous le tableau des articles.

### 3.4. Taxes et frais
Pour ajouter des taxes à votre devis, vous devez sélectionner un [Modèle de frais et taxes de vente](/fr/dokos/vente/modele-taxes) ou ajouter des taxes manuellement dans le tableau des taxes et frais de vente.

Le total des taxes et frais est affiché sous le tableau. En cliquant sur __Répartition des taxes__, vous afficherez toutes les composantes de taxes et leurs montants.

Vous pouvez également ajouter une [Règle de livraison](/fr/dokos/vente/regle-livraison) pour les articles dans votre devis.

### 3.5. Remise additionnelle
En plus d'offrir des remises par article, vous pouvez ajouter une remise à la totalité du devis dans cette section.  
Cette remise peut être basée sur le total TTC ou sur le total HT.  
La remise additionnelle peut être définie en pourcentage ou en montant.  

### 3.6. Termes de paiement
Parfois le paiement de la prestation n'est pas réalisé en une fois.  
Par exemple, 30% peut être payé avant l'envoi de la marchandise et le solde à la réception.  
Pour indiquer cela sur le devis, vous pouvez ajouter un modèle de termes de paiement ou ajouter des termes de paiement manuellement dans cette section.  

### 3.7. Termes et conditions
Vous pouvez sélectionner un modèle de termes et conditions et le modifier en fonction des spécificités de la transaction.

### 3.8. Paramètres d'impression
#### En-tête
Vous pouvez imprimer vos devis/commandes client avec l'en-tête de votre société.

'Grouper les articles identiques' groupera les articles identiques ajoutés plusieurs fois dans le tableau d'articles sur le format d'impression.

#### Titre de l'impression
Les devis peuvent aussi être intitulés "Facture proforma" ou "Proposition" par exemple.
Vous pouvez sélectionner le "Titre de l'impression" correspondant avant l'impression.

Pour créer un nouveau __Titre d'impression__, allez dans:
> Paramètres > Impression > Titre d'impression

### 3.9. Informations additionnelles
* **Campagne:** Une campagne de vente peut être associée à un devis. Plusieurs devis peuvent être associés à une campagne de vente.
* **Source:** La source de prospection peut être liée si le devis est fait à un prospect provenant de différents canaux (Campagne, Exposition,... ) ou bien être __Client existant__ si le devis est pour un client.  
* **Devis fournisseur:** Un devis fournisseur peut être lié à un devis afin de comparer les prix et avoir une idée du bénéfice/perte potentiel.

### 3.10. Valider le devis
Le devis est une transaction à "valider". Lorsque vous cliquez sur enregistrer, un brouillon est enregistré, qui est validé définitivement lorsque vous le validez.  
Puisque vous envoyez ce devis à votre client ou prospect, vous devez veillez à ce qu'aucun changement ne puisse lui être apporté après l'envoi du devis.  

Après la validation, vous pouvez créer un bon de commande ou une répétition automatique depuis le devis en utilisant le bouton "Créer". Dans le tableau de bord, au sommet du document, vous pouvez retrouver la commande client liée avec ce devis.  
Si votre devis n'aboutit pas à une vente, vous pouvez le considérer comme perdu en cliquant sur le bouton "Définir comme perdu". 
# Paramétrage du Site web

Paramétrez ici **l’affichage de votre site**, et définissez les options que vous voulez activer ou non.

### Effectuer le paramétrage de la page

1. Allez à: **Site web > Configuration > Paramètres du Site web**
2. Dans le champ “page d’accueil”, entrez “home” par défaut. (D’autres entrées incorrectes vous empêcheront de valider l’enregistrement de vos paramétrages).
3. Dans le champ “Thème du Site Web”, vous pouvez sélectionner les thèmes créés précédemment. (Site web > Configuration > Thème du Site Web)
4. Dans l’onglet “marque”, vous pouvez définir l’encadré qui contiendra votre logo de marque. 
5. Dans l’onglet “Barre supérieure”, vous pouvez définir ce que contiendra la barre de navigation en haut de votre site. Cliquez sur “Ajouter une Ligne”  pour ajouter un nouvel onglet dans votre barre de navigation. Le champ étiquette définit le nom de l’onglet, vous pouvez faire des sous-onglets en les plaçant dans un onglet parent. L’onglet parent ne doit pas avoir d’URL.

![Paramétrage du site web 1](.vuepress/public/images/website/website_settings/parametrage-site-web-1.png)
![Paramétrage du site web 2](.vuepress/public/images/website/website_settings/parametrage-site-web-2.png)
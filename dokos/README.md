## Welcome to dokos documentation website

dokos is an adaptation of a well-known management software: ERPNext.  
Benefiting from more than 10 years of development and an usage in thousands of companies, it is a robust and performant enterprise resource planning software.

dokos has been created to adapt this software to french and european norms and accelerate its development in Europe.

It is distributed under the GLPv3 licence.

---

dokos's documentation is under construction and you can participate to its enhancement by clicking on the link at the bottom of each page.

To navigate within the documentation, you can use the search field in the navigation bar of make a search by tag in the [Tags](/tags) page.

## Quick links

### General

- [Setting-up dokos](/dokos/setting-up/)

### Modules

- [Accounting](/dokos/accounting/)
- [Selling](/dokos/selling/)
- [Stocks](/dokos/stocks/)
- [Website](/dokos/website/)


---
Some content is adapted from [ERPNext documentation](https://erpnext.com)  
License: [CC 4.0 BY-SA-NC](https://creativecommons.org/licenses/by-nc-sa/4.0/)
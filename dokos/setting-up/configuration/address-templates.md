# Address templates

An address template is a display format of the different elements composing an address.

## 1. Why address templates

The display of addresses beeing different country-wise, it is important to be able to display an address differently if we send an invoice in France, Germany or China.

For this reason, dokos allows the configuration of an address template per country and to define one of these templates as a default one if a template doesn't exist for a given country.


## 2. Create a template

To create a new template, go to `Settings > Address template` and click on new.

1. Select the country for which this template is applicable.
1. Check the box `Is default` if you want to use this template as the default one for your system.
1. Format your template with the [Jinja](http://jinja.pocoo.org/docs/templates/) language or leave it empty if you want the system to add a standard template when you save.


## 3. Edit a template


Here is the standard template:

```
{{ address_line1 }}<br>{% if address_line2 %}{{ address_line2 }}<br>{% endif -%}{{ city }}<br>
{% if state %}{{ state }}<br>{% endif -%}
{% if pincode %}{{ pincode }}<br>{% endif -%}
{{ country }}<br>
{% if phone %}Phone: {{ phone }}<br>{% endif -%}
{% if fax %}Fax: {{ fax }}<br>{% endif -%}
{% if email_id %}Email: {{ email_id }}<br>{% endif -%}
```

Our address doesn't have any `address_line_2` or `fax` values, therefore the display will be: 

```
2045 Royal Road
St Paul
06570
Phone: 07911 123456
Email: hello@example.com
```

We would like to display the zip code on the left of the city name and remove the `state` field that is never used in the UK.
Therefore, we need to edit our template the following way:

```
{{ address_line1 }}<br>{% if address_line2 %}{{ address_line2 }}<br>{% endif -%}
{% if pincode %}{{ pincode }} {% endif -%}
{{ city }}<br>
{{ country }}<br>
{% if phone %}Phone: {{ phone }}<br>{% endif -%}
{% if fax %}Fax: {{ fax }}<br>{% endif -%}
{% if email_id %}Email: {{ email_id }}<br>{% endif -%}
```

The result will be:
```
2045 Royal Road
06570 St Paul
Phone: 07911 123456
Email: hello@example.com
```

All fields within the `Address` doctype are accessible in an address template. Even custom fields.
const getSettinUpSidebar = require('./sidebar/settingUp.js')
const getStockSidebar = require('./sidebar/stock.js')
const getWebsiteSidebar = require('./sidebar/website.js')
const getAccountingSidebar = require('./sidebar/accounting.js')
const getSellingSidebar = require('./sidebar/selling.js')
const getVersionsSidebar = require('./sidebar/versions.js')


module.exports = {
	title: 'dokos',
	description: 'Documentation for dokos',
	locales: {
		'/': {
		lang: 'en-US',
		title: 'dokos',
		description: 'Open-source platform for SME'
		},
		'/fr/': {
		lang: 'fr-FR',
		title: 'dokos',
		description: 'Plateforme open-source pour PME'
		}
	},
	head: [
		['link', { rel: 'icon', href: `/favicon.ico` }],
		['link', { rel: 'manifest', href: '/manifest.json' }],
		['meta', { name: 'theme-color', content: '#3eaf7c' }],
		['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
		['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
		['link', { rel: 'apple-touch-icon', href: `/icons/apple-touch-icon-152x152.png` }],
		['link', { rel: 'mask-icon', href: '/icons/safari-pinned-tab.svg', color: '#3eaf7c' }],
		['meta', { name: 'msapplication-TileImage', content: '/icons/msapplication-icon-144x144.png' }],
		['meta', { name: 'msapplication-TileColor', content: '#000000' }]
	],
	themeConfig: {
		logo: '/logo/dokos_logo_rect.svg',
		repo: 'https://gitlab.com/dokos/documentation',
		editLinks: true,
		algolia: {
			apiKey: 'ae1fcf2104324e91009179abc4b96849',
			indexName: 'dokos'
		},
		locales: {
			'/': {
				label: 'English',
				lastUpdated: 'Last Updated',
				selectText: 'Languages',
				editLinkText: 'Edit this page on Gitlab',
				lastUpdated: 'Last Updated',
				nav: require('./nav/en'),
				sidebar: {
					'/dokos/setting-up/': getSettinUpSidebar('en'),
					'/dokos/stocks/': getStockSidebar('Stocks', 'en'),
					'/dokos/website/': getWebsiteSidebar('Website', 'en'),
					'/dokos/accounting/': getAccountingSidebar('Accounting', 'en'),
					'/dokos/selling/': getSellingSidebar('Selling', 'en'),
					'/dokos/versions/': getVersionsSidebar('Versions', 'en')
				}
			},
			'/fr/': {
				label: 'Français',
				lastUpdated: 'Dernière mise à jour',
				selectText: 'Langues',
				editLinkText: 'Modifier cette page sur Gitlab',
				lastUpdated: 'Dernière mise à jour',
				nav: require('./nav/fr'),
				sidebar: {
					'/fr/dokos/installation/': getSettinUpSidebar('fr'),
					'/fr/dokos/stocks/': getStockSidebar('Stocks', 'fr'),
					'/fr/dokos/site-web/': getWebsiteSidebar('Site web', 'fr'),
					'/fr/dokos/comptabilite/': getAccountingSidebar('Comptabilité', 'fr'),
					'/fr/dokos/vente/': getSellingSidebar('Vente', 'fr'),
					'/fr/dokos/versions/': getVersionsSidebar('Versions', 'fr')
				}
			}
		}
	},
	plugins: [
		['@vuepress/active-header-links', true],
		['@vuepress/back-to-top', true],
		'@vuepress/nprogress',
		['@vuepress/medium-zoom', {
			selector: '.theme-default-content :not(a) > img',
			options: {
				margin: 16
			  }
		}],
		[
			'@vuepress/google-analytics',
			{
			  'ga': 'UA-35025071-5'
			}
		  ]
	]
}
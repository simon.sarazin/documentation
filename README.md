---
home: true
heroImage: /img/hero.svg
actionText: Get Started →
actionLink: /dokos/
footer: GPLv3 Licenced | Copyright © 2019 dokos
---
<div class="features">
  <div class="feature">
    <h2>Open Source</h2>
    <p>Fully own and control your company's technology</p>
  </div>
  <div class="feature">
    <h2>Comprehensive</h2>
    <p>More than 400 features for all your workflows</p>
  </div>
  <div class="feature">
    <h2>Flexible</h2>
    <p>Highly customizable to match your business requirements</p>
  </div>
</div>

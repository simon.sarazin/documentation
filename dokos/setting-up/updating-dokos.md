---
tags: ["update", "upgrade"]
---
# Updating dokos

Updating dokos is very straightforward but may necessitate some Linux/Python/NodeJS knowledge in case of error.

## The main command

Go to you bench folder (`home/{user}/{your_bench}) and run:

`bench update`


This command will do the following:

1. Put in application in maintenance mode
2. Update your application with the latest version on Gitlab
3. Update your python environment with any new dependency requirement
4. Run all patches and migrations necessary to update your database
5. Build all your javascript and CSS files
6. Put your application back in normal mode


## Decomposed commands

- `bench update --pull` will only pull the latest changes for each application
- `bench update --patch` will only run the patches and migrations for your database
- `bench update --build` will only build all JS and CSS files necessary for the user interface
- `bench update --bench` will only update the docli (Bench) application
- `bench update --requirements` will only update the python and nodejs dependencies in your environment

## Troubleshooting

If a patch breaks during the update, please post a new message on the [community forum](https://community.dokos.io) with a full copy of the error message. Wait for a correction, if so, and then run `bench update` again.

If you have custom applications in your bench folder, please make sure that they don't interfere with the standard update process.

Before reporting a potential issue, please test the update again in an environment without any custom application.
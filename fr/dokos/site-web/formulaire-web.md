# Formulaires web

Vous pouvez autoriser vos clients, fournisseurs ou postulants à accéder à certaines information ou à créer certaines transactions sur votre intance __dokos__.  
Par exemple: vous pouvez laisser n'importe qui se créer un compte sur votre site web (créé avec __dokos__ ou connecté via l'API) et postuler pour un emploi. Vous pouvez laisser vos clients voir le détail de leurs factures, leurs tickets de support ou leur donner la possibilité de télécharger un nouveau document eux-mêmes.

:::tip
Il y a deux types d'interfaces intégrées à __dokos__: Le *Bureau* et la *vue Web*.
    - Le Bureau est pour les utilisateurs au sein de votre organisation avec accès à des informations potentiellement confidentielles.
    - La vue Web est pour les utilisateurs en dehors de votre organisation qui ont besoin d'intéragir avec vous et à qui vous souhaitez fournir un 'self-service'.

Les formulaires web sont similaires aux formulaires que vous remplissez habituellement sur Internet. Ils font partie de la "vue Web".
:::

##  Créer un nouveau formulaire web

Pour créer un nouveau **Formulaire web**, allez à:

> Site web > Formulaire web

Sélectionnez un **DocType** qui servira de base pour construire votre formulaire web.  
Vous donnez simplement un accès contrôlé à ce type de document à vos utilisateurs et les autorisez à en modifier des parties ou à en créer un nouveau.  

La **Route** sera définie sur la base du **Titre** de votre formulaire web. Vous pouvez aussi ajouter un texte d'introduction pour afficher un message d'accueil au-dessus de votre formulaire.  

Ajoutez des champs à votre formulaire web.  
Ces champs sont ceux du type de document que vous avez sélectionné. Vous pouvez modifier les libellés de ces champs.  
Essayez de minimiser le nombre de champs car les formulaires trop longs sont ennuyeux à renseigner.  

Cliquez sur **Voir sur le site web** dans la barre latérale pour voir votre formulaire web.

Options sur la droite:

1. **Publié**: Le formulaire web ne sera accessible que si ceci cela est autorisé.
2. **Connexion obligatoire**: L'utilisateur peut renseigner le formulaire web uniquement s'il est connecté.  

    Lorsque Connexion Obligatoire est coché:  
    2.1. **Rediriger vers le lien de succès**: Redirige vers le lien de succès après validation du formulaire.  
    2.2. **Autoriser les modifications**: Si cette case n'est pas cochée, le formulaire sera en lecture seule après validation.  
    2.3. **Autoriser plusieurs documents**: Autorise l'utilisateur à créer plus d'un seul enregistrement.  
    2.4. **Afficher en grille**: Affiche les enregistrements dans un tableau.  
    2.5. **Autoriser les suppressions**: Autoriser l'utilisateur à supprimer les enregistrements qu'il/elle a créé·e.  
    2.6. **Autoriser les commentaires**: Autoriser l'utilisateur à ajouter des commentaires sur le formulaire créé.  

9. **Autoriser les impressions**: Autoriser l'utilisateur à imprimer le document avec le format d'impression sélectionné.
10. **Autoriser les formulaires incomplets**: Autoriser l'utilisateur à soumettre un formulaire avec des données partielles.

## 2. Fonctionnalités
### 2.1 Barre latérale

Vous pouvez aussi afficher des liens contextuels dans une barre latérale sur votre formulaire web.

### 2.2 Créer des formulaires web avec tables enfants.

Vous pouvez ajouter des tables enfants à vos formulaires web, comme dans les formulaires classiques.


### 2.3 Intégration de passerelles de paiement

Vous pouvez ajouter une passerelle de paiement à un formulaire web.  
Lorsque vos utilisateurs enregistrent leur formulaire, ils sont redirigés vers un formulaire de paiement.

### 2.4 Utilisateur de portail

Dans les paramètres de portail, vous pouvez définir un rôle pour chaque élément de menu pour que seuls les utilisateurs avec ce rôle puissent voir cet élément.


### 2.5 Scripts personnalisés

Vous pouvez écrire des scripts personnalisés pour vos formulaires web afin de valider les entrées, pré-remplir les champs, afficher un message de succès ou toute autre action personnalisée.

### 2.6 Actions

Vous pouvez ajouter un texte dans le champ 'Message de succès' qui sera ensuite affiché à l'utilisateur après validation du formulaire web.  
L'utilisateur sera redirigé vers l'URL définie dans le champ 'Lien de succès' en cliquant sur le bouton 'Continuer'.  
Ceci est valable uniquement pour les utilisateurs non enregistrés (formulaires web avec le bouton 'Connexion requise' non coché).  


### 2.7 Résultat

Lorsqu'un utilisateur de site web soumet son formulaire, les données sont enregistrées dans le type de document auquel est lié ce formulaire.  
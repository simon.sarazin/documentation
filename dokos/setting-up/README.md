---
tags: ["setting up"]
---
# Installation
<TagLinks />

- This is an opinionated setup meant to be run on a blank server.
- Works on Ubuntu 16.04+, CentOS 7+, Debian 8+
- You may have to install Python 2.7 (eg on Ubuntu 16.04+) by running `apt-get install python-minimal`
- You may also have to install build-essential and python-setuptools by running `apt-get install build-essential python-setuptools`
- This script will install the pre-requisites, install docli and setup a dokos site
- Passwords for the System Administrator and MariaDB (root) will be asked
- You can then login as **Administrator** with the Administrator password

Open your Terminal and enter:

#### 1. Download the install script

For Linux:

    wget https://gitlab.com/dokos/docli/raw/master/playbooks/install.py


#### 2. Run the install script

If you are on a fresh server and logged in as **root**, at first create a dedicated user for dokos
& equip this user with **sudo** privileges

```
  adduser [dokos-user]
  usermod -aG sudo [dokos-user]
```

_(For the security of your installation, don't name your user `dokos`)

Running the script for production:

    sudo python install.py --production --user [dokos-user]


::: tip
The applications are named Frappe and ERPNext because dokos is an adaptation of these software.  
The underlying architecture is similar to these two software.  
You can get more info on their respective documentation sites: [Frappe](https://frappe.io/docs), [ERPNext](https://erpnext.com/docs)
:::

#### What will this script do?

- Install all the pre-requisites
- Install the command line `bench`
- Create a new bench (a folder that will contain your entire dokos setup)
- Create a new dokos site on the bench

#### How do I start dokos

Your process will be automatically setup and managed by `nginx` and `supervisor`.
If that's not the case, you can run:

    sudo bench setup production [dokos-user]

You can then connect to your server's address to start utilizing dokos.

::: tip Astuce
If your server's address is 57.69.123.1, enter it in your browser to start using dokos.
:::

---

Help
====

For bench help, you can type

    bench --help

Updating
========

To manually update the bench, run `bench update` to update all the apps, run
patches, build JS and CSS files and restart supervisor.

You can also run the parts of the bench selectively.

`bench update --pull` will only pull changes in the apps

`bench update --patch` will only run database migrations in the apps

`bench update --build` will only build JS and CSS files for the bench

`bench update --bench` will only update the bench utility (this project)

`bench update --requirements` will only update dependencies (python packages) for the apps installed
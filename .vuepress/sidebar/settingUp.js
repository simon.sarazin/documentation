module.exports = function getSidebar (lang) {
	if (lang === 'fr') {
		return [
			{
				title: 'Démarrage',
				collapsable: false,
				children: [
					'',
					'migration-erpnext',
					'mettre-a-jour-dokos'
				]
			},
			{
				title: 'Configurer dokos',
				collapsable: false,
				children: [
					'configuration/configuration-email',
					'configuration/formats-impression',
					'configuration/modele-adresses'
				]
			},
			{
				title: 'Personnaliser dokos',
				collapsable: false,
				children: [
					'personnalisation/scripts-python'
				]
			},
			{
				title: 'Intégrations',
				collapsable: false,
				children: [
					'integrations/google',
					'integrations/zapier'
				]
			}
		]
	}
	return [
		{
			title: 'Setup',
			collapsable: false,
			children: [
				'',
				'erpnext-migration',
				'updating-dokos'
			]
		},
		{
			title: 'Configure dokos',
			collapsable: false,
			children: [
				'configuration/email-setup',
				'configuration/print-formats',
				'configuration/address-templates'
			]
		},
		{
			title: 'Customize dokos',
			collapsable: false,
			children: [
				'customization/server-scripts'
			]
		},
		{
			title: 'Integrations',
			collapsable: false,
			children: [
				'integrations/google',
				'integrations/zapier'
			]
		}
	]
}
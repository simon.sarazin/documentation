# Passerelles de paiement

Les passerelles de paiement sont des documents permettant de créer un lien entre les documents de facturation et de paiement de Dokos avec des intermédiaires de paiement comme Stripe, Braintree, GoCardless, Paypal, etc.


### 1. Structures des passerelles de paiement

Une nouvelle passerelle de paiement est automatiquement créée lorsque vous configurez une nouvelle intégration avec un intermédiaire de paiement dans Dokos.
Exemple: lorsque vous ajoutez un nouveau compte Stripe appelé `ABC`, une passerelle de paiement appelée `Stripe-ABC` est automatiquement créée dans Dokos.

Une passerelle de paiement est ensuite liée à un ou plusieurs comptes comptables, un compte pour chaque devise utilisée pour payer.
Exemple : si vous souhaitez traiter des paiements en EUR ou USD, vous devez créer deux comptes de passerelle de paiement liés à votre passerelle de paiement :
- Un compte de passerelle de paiement avec la devise EUR, lié à un compte comptable en EUR
- Un compte de passerelle de paiement avec la devise USD, lié à un compte comptable en USD

Chaque fois qu'un paiement est effectué via cette passerelle de paiement, le système comptabilise l'écriture de paiement sur le bon compte de passerelle de paiement.


### 2. Configuration

Il est possible de définir les éléments suivants pour chaque passerelle de paiement:

- Pour la génération automatique des écritures de paiement:
    - Un mode de paiement
    - Un compte de frais : utilisé pour comptabiliser les frais prélevés par l'intermédiaire de paiement
    - Un centre de coût

- Pour le portail:
    - Un titre
    - Une icône

Exemple :
![dokos payment page](/images/accounting/payment_request/payment_request_link.png)

### 3. Intégrations
#### 3.1 Stripe
##### 3.1.1 Configurer Stripe

Afin de configurer une intégration avec Stripe, vous devez ajouter les éléments suivants :

- Créez un nouveau document `Paramètres Stripe` pour chacune de vos sociétés
  > Allez dans `Intégrations > Paramètres Stripe`
- Ajoutez la clé publiable
- Ajoutez la clé secrète
  > Ces clés peuvent être trouvées dans la section `Développeur > Clés API` du tableau de bord Stripe.
- Ajoutez une image d'en-tête : elle sera affichée au-dessus du formulaire de paiement sur votre portail, mais ce n'est pas obligatoire.
- Ajoutez une URL de redirection si différente de l'URL de redirection standard.
  > Après un paiement réussi, l'utilisateur est redirigé vers une page de `Succès de paiement` (`/integrations/payment-success`) contenant un bouton `continuer`. Vous pouvez définir le lien de redirection de ce bouton dans ce champ.

##### 3.1.2 Webhooks

Il est aussi possible de recevoir des webhooks de Stripe pour générer des actions de facturation automatiquement.
Afin d'activer les webhooks, allez dans votre tableau de bord Stripe, sélectionnez `Webhooks` dans la section `Développeur` et ajoutez un nouveau `endpoint` avec l'URL suivante:

`https://{votre_site}/api/method/erpnext.erpnext_integrations.doctype.stripe_settings.webhooks?account={nom_de_votre_compte_stripe}`

Remplacez {votre_site} et {nom_de_votre_compte_stripe} par l'URL de votre site (exemple : `myerp.dokos.io`) et le nom de votre compte Stripe dans Dokos (exemple : `ABC`).

Vous pouvez envoyer tous les événements liés au document :
    - invoice

Copiez ensuite la clé secrète et collez la dans votre compte Stripe sur Dokos.

##### 3.1.3 Après la création

Une fois votre compte Stripe créé, allez dans les passerelles de paiement générées et complétez les informations présentées en section 2.
Si vous devez autoriser des paiements dans d'autres devises que la devise de votre société, ajoutez des comptes de passerelle de paiement additionnels.

Dokos est configuré pour accepter les devises suivantes dans Stripe :
```
"AED", "ALL", "ANG", "ARS", "AUD", "AWG", "BBD", "BDT", "BIF", "BMD", "BND",
"BOB", "BRL", "BSD", "BWP", "BZD", "CAD", "CHF", "CLP", "CNY", "COP", "CRC", "CVE", "CZK", "DJF",
"DKK", "DOP", "DZD", "EGP", "ETB", "EUR", "FJD", "FKP", "GBP", "GIP", "GMD", "GNF", "GTQ", "GYD",
"HKD", "HNL", "HRK", "HTG", "HUF", "IDR", "ILS", "INR", "ISK", "JMD", "JPY", "KES", "KHR", "KMF",
"KRW", "KYD", "KZT", "LAK", "LBP", "LKR", "LRD", "MAD", "MDL", "MNT", "MOP", "MRO", "MUR", "MVR",
"MWK", "MXN", "MYR", "NAD", "NGN", "NIO", "NOK", "NPR", "NZD", "PAB", "PEN", "PGK", "PHP", "PKR",
"PLN", "PYG", "QAR", "RUB", "SAR", "SBD", "SCR", "SEK", "SGD", "SHP", "SLL", "SOS", "STD", "SVC",
"SZL", "THB", "TOP", "TTD", "TWD", "TZS", "UAH", "UGX", "USD", "UYU", "UZS", "VND", "VUV", "WST",
"XAF", "XOF", "XPF", "YER", "ZAR"
```


#### 3.2 GoCardless
##### 3.1.1 Configurer GoCardless

Afin de configurer une intégration avec GoCardless, vous devez ajouter les éléments suivants :

- Créez un nouveau document `Paramètres GoCardless` pour chacune de vos sociétés
  > Allez dans `Intégrations > Paramètres GoCardless`
- Ajoutez le jeton d'accès
  > Ce jeton peut être généré dans la section `Développeurs > Créer > Jeton d'accès` du tableau de bord GoCardless.
- Cochez `Utiliser la sandbox` s'il s'agit d'un compte de test GoCardless.

##### 3.1.2 Webhooks

Pour intégrer les webhooks GoCardless dans Dokos, créez un nouveau `endpoint` en cliquant sur `Développeurs > Créer > Endpoint de webhook`.
Donnez lui un nom logique et ajoutez l'URL suivante:

`https://{votre_site}/api/method/erpnext.erpnext_integrations.doctype.gocardless_settings.webhooks?account={nom_de_votre_compte_stripe}`

Remplacez {votre_site} et {nom_de_votre_compte_stripe} par l'URL de votre site (exemple : `myerp.dokos.io`) et le nom de votre compte GoCardless dans Dokos (exemple : `ABC`)

Vous pouvez laisser le champ `Secret` libre, car il sera automatiquement généré par GoCardless.

Enregistrez, puis copiez-collez le `Secret` généré dans vos paramètres GoCardless dans Dokos.

##### 3.1.3 Vérifier les mises à jour

En cochant `Vérifier les mises à jour`, vous autorisez une vérification quotidienne des éléments suivants:

    - Le statut de chaque mandat est mis à jour si un changement est enregistré par GoCardless
    - Le statut de chaque paiement est mis à jour si un changement est enregistré par GoCardless

Ces deux vérifications se font en plus des modifications effectuées par les webhooks reçus de GoCardless.